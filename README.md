# PHP + Caddy + Prometheus

All in one container, not exactly a best-practice, but the only
viable way we found to make blue-green deployments with php apps

## What is

A single image for:

- [wodby/php:7.4](https://github.com/wodby/php) 
- [php-fpm healthcheck](https://github.com/renatomefi/php-fpm-healthcheck)
- [php-fpm-exporter](https://github.com/hipages/php-fpm_exporter)
- [caddy](https://github.com/caddyserver/caddy)

All managed by [ochinchina/supervisord](https://github.com/ochinchina/supervisord)

## test

```
docker-compose up -d --build
```

Check urls:
- PHPinfo [/php-status](http://localhost:50080/php-status)
- Supervisor status page: [:9003](http://localhost:59003)
- php metrics [:9253](http://localhost:59253/metrics)

## CLI Interface

```
docker-compose exec supervisord ctl status
docker-compose exec supervisord ctl stop php
docker-compose exec supervisord ctl start php
docker-compose exec supervisord ctl stop caddy 
docker-compose exec supervisord ctl start caddy 
```

See [manual of supervisord](https://github.com/ochinchina/supervisord) for more commands
