# prepare the final image
FROM wodby/php:7.4-4.33.4

USER root
# allow php to terminate gracefully during deployments
# https://www.goetas.com/blog/traps-on-the-way-of-blue-green-deployments/
RUN sed -i 's/;process_control_timeout = 0/process_control_timeout = 1m/' /usr/local/etc/php-fpm.conf
USER wodby

# Add supervisor to manage exporter and php-fpm processes alltogether
COPY supervisord.conf /etc/supervisord.conf
COPY --from=registry.gitlab.com/opencity-labs/supervisord:v0.7.3 /usr/local/bin/supervisord \
        /usr/local/bin/supervisord
EXPOSE 9003


# Add utility to check healthness of php-fpm
# remember to add PHP_FPM_PM_STATUS_PATH env variable to /php-status
ENV FCGI_STATUS_PATH=/php-status
RUN curl https://raw.githubusercontent.com/renatomefi/php-fpm-healthcheck/master/php-fpm-healthcheck > /usr/local/bin/php-fpm-healthcheck && \
    chmod +x /usr/local/bin/php-fpm-healthcheck
RUN echo -e "[www]\npm.status_path = /php-status\n" > /usr/local/etc/php-fpm.d/zz-status.conf

HEALTHCHECK --interval=15s --timeout=3s \
        CMD /usr/local/bin/php-fpm-healthcheck

COPY --from=hipages/php-fpm_exporter:latest /php-fpm_exporter /bin/php-fpm_exporter
EXPOSE 9253

COPY --from=caddy:2.6.4-alpine /usr/bin/caddy /usr/bin/caddy
COPY Caddyfile /etc/caddy/Caddyfile
EXPOSE 80 443

COPY app.php /var/www/html/web/

#ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/local/bin/supervisord"]

